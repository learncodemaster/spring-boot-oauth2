package com.ults.IPMS.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ults.IPMS.entity.Permission;
import com.ults.IPMS.entity.RoleMenuAccess;


@JsonInclude(Include.NON_EMPTY)
public class MenuDTO{

	private String menuName;

	private String url;

	private String description;

	private long menuID;
	
	private boolean isMainMenu;
	
	private Set<MenuDTO> menu=new HashSet<MenuDTO>();
	
	//private Menu parantMenu;

	private Set<Permission> permissions = new HashSet<Permission>();
	
	
	

	public MenuDTO() {
		super();
	}

	public MenuDTO(RoleMenuAccess roleMenuAccess) {
		super();
		this.menuName = roleMenuAccess.getMenu().getMenuName();
		this.url = roleMenuAccess.getMenu().getUrl();
		this.description = roleMenuAccess.getMenu().getDescription();
		this.menuID = roleMenuAccess.getMenu().getId();
		this.isMainMenu =roleMenuAccess.getMenu().isMainMenu();
		if (roleMenuAccess.getMenu().getSubMenus().size() > 0) {
			roleMenuAccess.getMenu().getSubMenus().forEach(x -> {
				MenuDTO menuDTO = new MenuDTO();
				menuDTO.setMenuID(x.getId());
				menuDTO.setMenuName(x.getMenuName());
				menuDTO.setUrl(x.getUrl());
				menuDTO.setDescription(x.getDescription());
				menuDTO.setMainMenu(x.isMainMenu());
				this.menu.add(menuDTO);
			});

		}
		roleMenuAccess.getPermission().forEach(p->{
			this.permissions.add(p);
		});
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getMenuID() {
		return menuID;
	}

	public void setMenuID(long menuID) {
		this.menuID = menuID;
	}
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Set<MenuDTO> getMenu() {
		return menu;
	}

	public void setMenu(Set<MenuDTO> menu) {
		this.menu = menu;
	}

	public boolean isMainMenu() {
		return isMainMenu;
	}

	public void setMainMenu(boolean isMainMenu) {
		this.isMainMenu = isMainMenu;
	}

//	public Menu getParantMenu() {
//		return parantMenu;
//	}
//
//	public void setParantMenu(Menu parantMenu) {
//		this.parantMenu = parantMenu;
//	}





	
}
