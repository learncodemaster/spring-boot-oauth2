package com.ults.IPMS.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import com.ults.IPMS.entity.AdditionalDetails;
import com.ults.IPMS.entity.Establishment;
import com.ults.IPMS.entity.Lookup;
import com.ults.IPMS.entity.MemberDetails;
import com.ults.IPMS.entity.OwnerBuildingProperty;
import com.ults.IPMS.entity.OwnerDetails;
import com.ults.IPMS.entity.Property;
import com.ults.IPMS.entity.PropertyRoad;
import com.ults.IPMS.entity.PropertyRoof;
import com.ults.IPMS.entity.PropertyTax;
import com.ults.IPMS.entity.PropertyWaste;
import com.ults.IPMS.entity.Tenant;

public class SurveyResponseDTO {


	private boolean accountNo;

	private int basementDetails;

	private boolean bathroom;

	private String bldgName;

	private String bldgStatus;

	private String bldgSubtyp;

	private String bldgtype;

	private String bldgUnder;

	private String bldgUsage;

	private String zone;

	private boolean carPorch;

	private int carPorchArea;

	private int cattles;

	private boolean centralAc;

	private String commonRemarks;

	private boolean commonStair;

	private int commonStairArea;

	private String consumerNo;

	private boolean cultivatedLand;

	private String district;

	private String doorStatus;

	private boolean electricity;

	private String noOfeEmploy;

	private String establishmentEmail;

	private String establishmentLandline;

	private String establishmentMobile;

	private String establishmentName;

	private String establishmentType;

	private String establishmentYear;

	private boolean financialSupport;

	private boolean fourWheeler;

	private Set<String> floorType=new HashSet<String>();//*/

	//private String fourWheelerType;

	private boolean gasConnection;

	private int grndFloor;

	private boolean healthInsurance;

	private double floorSqmtr;

	private String incharge;

	private String inchargero;

	private String informedby;

	private boolean kudumbasree;

	private boolean kwaWater;

	private boolean landmark;

	private String landmarkcategry;

	private String landmarkname;

	private String landmarksubtype;

	private String latitude;

	private boolean latrine;

	private String licenceNo;

	private String localbody;

	private String longitude;

	private Set<MemberDetails> memberDetails = new HashSet<MemberDetails>();

	private String nearProno;

	private String nearRoad;

	private long newProid;

	private int noOfCattles;

	private int noOfFloors;

	private String noOfTypes;

	private int noOfRooms;

	private int oldProid;

	private String otherBldg;

	private String ownerEmail;

	private String ownerHouseName;

	private String ownerLand;

	private String ownerPhone;

	private String ownerName;

	private String ownerOccup;

	private int ownerPincode;

	private String ownerPostoffice;

	private String ownerState;

	private String ownerPlace;

	private boolean poultry;

	private boolean petdog;

	private String locationPincode;

	private String place;

	private String plotArea;

	private String ptStatus;

	private String postoffice;

	private String propertyCount;

	private long propertyId;

	private String propertyImage1;

	private String propertyImage2;

	private boolean rainWaterHarvest;

	private boolean rationcard;

	private String rationca1;

	private String religion;

	private String castName;

	private String remarks;

	private String roadtype;

	private String roadwidth;

	private String roofType;

	private double roofTotper;

	private String street;

	private String structureType;

	private String globalId;

	private long surveyNo;

	private double taxAmount;

	private double annualTaxAmount;

	private Date taxPaidDate;


	private String taxNumber;

	private int taxPaidYear;

	private String rentAmout;

	private String tenantEmail;

	private String tenantHouseName;


	private String tenantLand;

	private String tenantPhone;

	private String tenantName;

	private String tenantNative;

	private int tenantPincode;


	private String tenantPostoffice;

	private String tenantState;

	private String tenantStreet;

	private boolean thozhilurappu;

	private int noOfYears;


	private String uniqueId;

	private boolean uncultivatedLand;

	private String version;

	private String villageName;

	private Set<String> wallType=new HashSet<String>();

	private String wardName;

	private String wardNo;

	private String waste_managment;

	private Set<String> wasteMangmentType=new HashSet<String>();

	private Set<String> waterSourceType=new HashSet<String>();

	private boolean wellPerennial;

	private String wellPerennialMonth;

	private int yearOfCons;

	public SurveyResponseDTO() {
		super();
	}

	public SurveyResponseDTO(Property property) {
		super();
		this.accountNo = property.getLivelihoodDetail().isBankAccavailable();
		this.basementDetails = property.getBuilding().getBasementCount();
		this.bathroom = property.getLivelihoodDetail().isBathroomAvailable();
		this.bldgName = property.getBuilding().getBuildingName();
		this.bldgStatus = property.getPropertyStatus().getDisplayValue();
		this.bldgSubtyp = property.getPropertySubType().getDisplayValue();
		this.bldgtype = property.getPropertyType().getDisplayValue();
		this.bldgUnder = property.getBuildingUnder().getDisplayValue();
		this.bldgUsage = property.getPropertyUsageType().getDisplayValue();
		this.zone = property.getBuilding().getBuildingZone().getDisplayValue();
		this.carPorch = property.isCarporchAvailbale();
		this.carPorchArea = property.getCarporchArea();
		this.cattles = property.getLivelihoodDetail().getCattkeCount();
		this.centralAc = property.isAcAvailable();
		this.commonRemarks = property.getRemarks();
		this.commonStair = property.isCommonStairCaseAvailbale();
		this.commonStairArea = property.getCommonStairCaseArea();
		this.consumerNo = property.getConsumerNumber();
		AdditionalDetails additionalDetails= property.getAdditionalDetails().stream().findFirst().get();
		this.cultivatedLand = additionalDetails.isCultivatedLand();
		this.district = property.getBuilding().getLocation().getDistrict().getDistrictName();
		this.doorStatus = property.getDoorStatus().getDisplayValue();
		this.electricity = property.isElectricalConnection();
		Establishment establishment=property.getEstablishment().stream().findFirst().get();
		this.noOfeEmploy = establishment.getEmpCount();
		this.establishmentEmail =establishment.getEmail();
		this.establishmentLandline = establishment.getLandlineNo();
		this.establishmentMobile = establishment.getMobileNo();
		this.establishmentName = establishment.getEstName();
		this.establishmentType = establishment.getEstablishmentType().getDisplayValue();
		this.establishmentYear = establishment.getEstYear();
		this.financialSupport = additionalDetails.isPropertyFinancialSupport();
		this.fourWheeler = additionalDetails.isHasFourWheeler();
		property.getFloorType().forEach(floor->{
			this.floorType.add(floor.getDisplayValue());
		});
		
		this.gasConnection = property.getLivelihoodDetail().isGasConnAvailable();
		
		property.getBuilding().getBuildingFloor().forEach(floorDetails->{
			switch (floorDetails.getFloorNumber()) {
			case 0:
				this.grndFloor = floorDetails.getFloorArea();
				break;

			default:
				this.floorSqmtr = floorDetails.getFloorArea();
				break;
			}
		});
		
		this.healthInsurance = additionalDetails.isHealthInsurance();
		
		this.incharge = establishment.getEstInCharge();
		this.inchargero = establishment.getInChargeRole();
		this.informedby = property.getInformedBy();
		this.kudumbasree = additionalDetails.isKudumbasree();
		this.kwaWater = property.getLivelihoodDetail().isWaterConnAvailable();
		this.landmark = property.isLandmark();
		this.landmarkcategry = property.getLandmark().getCategory().getDisplayValue();
		this.landmarkname = property.getLandmark().getPlaceName();
		this.landmarksubtype = landmarksubtype;
		this.latitude = latitude;
		this.latrine = property.isLatrineAvailable();
		//this.licenceNo = licenceNo;
		this.localbody = localbody;
		this.longitude = longitude;
		this.memberDetails = property.getMemberDetail();
		//this.nearProno = nearProno;
		PropertyRoad propertyRoad=property.getPropertyRoad().stream().findFirst().get();
		this.nearRoad = propertyRoad.getNearRoadName();
		this.newProid = property.getNewId();
		this.noOfCattles = property.getLivelihoodDetail().getCattkeCount();
		this.noOfFloors = property.getBuilding().getFloorCount();
		//this.noOfTypes = noOfTypes;
		this.noOfRooms = property.getRoomCount();
		this.oldProid = property.getOldId();
		//this.otherBldg = otherBldg;
		OwnerDetails ownerDetails= property.getOwnerDetails().stream().findFirst().get();
		this.ownerEmail = ownerDetails.getEmail();
		this.ownerHouseName = ownerDetails.getHouseName();
		this.ownerLand = ownerDetails.getLandlineNo(); 
		this.ownerPhone = ownerDetails.getMobileNo();
		this.ownerName = ownerDetails.getOwnerName();
		this.ownerOccup = ownerDetails.getOwnerOccupation();
		this.ownerPincode = ownerDetails.getPincode();
		this.ownerPostoffice = ownerDetails.getPostOffice();
		//this.ownerState = ownerDetails.get;// no state
		//this.ownerPlace = ownerDetails.get;//no place
		this.poultry = property.getLivelihoodDetail().isHasPoultry();
		this.petdog = property.getLivelihoodDetail().isHasPetDog();
		this.locationPincode = property.getBuilding().getLocation().getPincode();
		this.place = property.getBuilding().getLocation().getPlaceName();
		this.plotArea = additionalDetails.getPlotArea();
		//this.ptStatus = ;
		this.postoffice = property.getBuilding().getLocation().getPostOffice();
		//this.propertyCount = property.getp;
		this.propertyId = property.getId();
		this.propertyImage1 = property.getPhoto1();
		this.propertyImage2 = property.getPhoto2();
		this.rainWaterHarvest = property.getLivelihoodDetail().isRainWaterHarvesting();
		this.rationcard = property.getLivelihoodDetail().isRaionCardAvailable();// douth in color or available
		this.rationca1 = property.getLivelihoodDetail().getRationCardNum();
		this.religion = property.getLivelihoodDetail().getReligion().getDisplayValue();
		this.castName = property.getLivelihoodDetail().getCast().getDisplayValue();
		this.remarks = property.getRemarks(); 
		this.roadtype = propertyRoad.getRoadType().getDisplayValue();
		this.roadwidth = propertyRoad.getRoadWidth().getDisplayValue();
		PropertyRoof propertyRoof=property.getPropertyRoof().stream().findFirst().get();
		this.roofType = propertyRoof.getRoofType().getDisplayValue();
		this.roofTotper = propertyRoof.getRoofPercent();
		this.street = property.getBuilding().getLocation().getStreetName();
		this.structureType = property.getStructureType().getDisplayValue();
	//	this.globalId = property.getUid();
		this.surveyNo = property.getSurveyNumber();
		PropertyTax propertyTax=property.getPropertyTax().stream().findFirst().get();
		this.taxAmount = propertyTax.getTaxAmount();
		this.annualTaxAmount = propertyTax.getAnnualTaxAmount();
		this.taxPaidDate = propertyTax.getTaxPaidDate();
		this.taxNumber = propertyTax.getTaxReceiptNumber();
		this.taxPaidYear = propertyTax.getTaxPaidYear();
		
		Tenant tenant=property.getTenant().stream().findFirst().get();
		this.rentAmout = tenant.getRentAmount();
		this.tenantEmail = tenant.getEmail() ;
		this.tenantHouseName = tenant.getHouseNmae();
		this.tenantLand = tenant.getLandlineNo(); 
		this.tenantPhone = tenant.getMobileNo();
		this.tenantName = tenant.getTenantName();
		this.tenantNative = tenant.getNativePlace();
		this.tenantPincode = tenant.getPincode();
		this.tenantPostoffice = tenant.getPostOffice();
		//this.tenantState = tenant.g;//no state for tenant
		this.tenantStreet = tenant.getStreetName();
		this.thozhilurappu = additionalDetails.isThozhilurapp();
		this.noOfYears = property.getYearsCount();
		this.uniqueId = property.getUid();
		this.uncultivatedLand = additionalDetails.isUncultivated();
		this.version = version;
		this.villageName = property.getBuilding().getLocation().getVillage().getVillageName();
		//this.wallType = property.getWallTypeId();// same as id or??
		this.wardName = property.getBuilding().getLocation().getWardName();
		this.wardNo = property.getBuilding().getLocation().getWardNumber();
	//	this.waste_managment = property.getLivelihoodDetail().;
		PropertyWaste propertyWaste=property.getPropertyWaste().stream().findFirst().get();
		//this.wasteMangmentType = propertyWaste.getWaterManagementType().getDisplayValue();
	//	this.waterSourceType = property.getPropertyWaste().stream().findFirst().get().getWaterManagementType().get;
		this.wellPerennial = additionalDetails.isHasWellPerenialDetails();
		this.wellPerennialMonth = additionalDetails.getPerenialMonth();
		this.yearOfCons = property.getYearOfConstruction();
	}



	

}
