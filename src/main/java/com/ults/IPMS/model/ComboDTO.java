package com.ults.IPMS.model;

import java.util.HashSet;
import java.util.Set;

public class ComboDTO {

	private long id;

	private String content;

	private Set<ComboDTO> subContent=new HashSet<ComboDTO>();

	public ComboDTO() {
		super();
	}

	public ComboDTO(long id, String content) {
		super();
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Set<ComboDTO> getSubContent() {
		return subContent;
	}

	public void setSubContent(Set<ComboDTO> subContent) {
		this.subContent = subContent;
	}

	

}
