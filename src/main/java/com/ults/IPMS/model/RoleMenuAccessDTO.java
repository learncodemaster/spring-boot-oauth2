package com.ults.IPMS.model;

public class RoleMenuAccessDTO {
	
	private Long id;

	private MenuDTO menu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MenuDTO getMenu() {
		return menu;
	}

	public void setMenu(MenuDTO menu) {
		this.menu = menu;
	}

	
	
	
}
