package com.ults.IPMS.model;

import com.ults.IPMS.entity.UserAccess;

public class LoginResponseDTO {
	
	private long userAccessId;
	
	private String username;
	
	private String firstName;
	
	private String email;
	
	private String role;
	
	private String contactNo;
	
	

	public LoginResponseDTO() {
		super();
	}
	
	

	public LoginResponseDTO(UserAccess userAccess) {
		super();
		this.userAccessId = userAccess.getId();
		this.username = userAccess.getUsername();
		this.firstName = userAccess.getFirstName();
		this.email = userAccess.getEmail();
		this.role = userAccess.getRole().getRole_name();
		this.contactNo = userAccess.getContact_no();
	}



	public long getUserAccessId() {
		return userAccessId;
	}

	public void setUserAccessId(long userAccessId) {
		this.userAccessId = userAccessId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	
	
	
	
	

}
