package com.ults.IPMS.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.ults.IPMS.entity.UserAccess;
import com.ults.IPMS.model.LoginResponseDTO;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public CustomToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<>(); 
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
		UserAccess user=customUserDetails.getUserAccess(); 
		LoginResponseDTO  login = new LoginResponseDTO(user);
        additionalInfo.put("details", login);
        additionalInfo.put("jti", "");
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        CustomToken token = new CustomToken(accessToken);
        return token;
    }
}