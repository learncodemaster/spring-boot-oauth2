package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Role;

@Repository
public interface RoleDAO extends JpaRepository<Role, Integer>{



}
