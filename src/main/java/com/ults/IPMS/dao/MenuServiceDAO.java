package com.ults.IPMS.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Menu;

@Repository
public interface MenuServiceDAO extends JpaRepository<Menu,Long>{

Set<Menu> findByisMainMenu(boolean status);
	

}
