package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.RoleLevel;

@Repository
public interface RoleLevelDAO extends JpaRepository<RoleLevel, Integer>{

	

}
