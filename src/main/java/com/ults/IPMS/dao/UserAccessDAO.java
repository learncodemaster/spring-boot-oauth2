package com.ults.IPMS.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.UserAccess;
@Repository
public interface UserAccessDAO extends JpaRepository<UserAccess, Long>{
  
	Optional<UserAccess> findByUsername(String userName);
	
	//UserAccess findById(long id);

	boolean existsByUsername(String username);
}  
    