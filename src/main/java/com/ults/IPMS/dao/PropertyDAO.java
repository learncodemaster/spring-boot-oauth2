package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Property;

@Repository
public interface PropertyDAO extends JpaRepository<Property, Long>{

}
