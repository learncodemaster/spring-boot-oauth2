package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.OauthClientDetails;


@Repository
public interface OauthClientDetailsDAO extends JpaRepository<OauthClientDetails, String>{

}
