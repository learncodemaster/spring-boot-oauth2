package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.LoginHistory;

@Repository
public interface LoginHistoryDAO extends JpaRepository<LoginHistory, Long>{

}
    