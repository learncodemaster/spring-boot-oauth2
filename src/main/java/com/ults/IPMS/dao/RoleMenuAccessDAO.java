package com.ults.IPMS.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Role;
import com.ults.IPMS.entity.RoleMenuAccess;

@Repository
public interface RoleMenuAccessDAO extends JpaRepository<RoleMenuAccess,Long>{

	Set<RoleMenuAccess> findByRole(Role role);
	

}
