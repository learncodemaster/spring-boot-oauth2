package com.ults.IPMS.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Lookup;
import com.ults.IPMS.entity.LookupType;

@Repository	
public interface LookupDAO extends JpaRepository<Lookup, Long>{
	
	Set<Lookup> findByLookupType(LookupType lookupType);

}
