package com.ults.IPMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.Building;

@Repository
public interface BuildingDAO extends JpaRepository<Building, Long> {

}
