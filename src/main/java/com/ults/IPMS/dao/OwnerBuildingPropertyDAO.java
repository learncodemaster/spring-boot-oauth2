package com.ults.IPMS.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.OwnerBuildingProperty;
import com.ults.IPMS.entity.Property;

@Repository
public interface OwnerBuildingPropertyDAO extends JpaRepository<OwnerBuildingProperty, Long>{
	Optional<OwnerBuildingProperty> findByProperty(Property property);
}
