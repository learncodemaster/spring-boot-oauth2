package com.ults.IPMS.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ults.IPMS.entity.LookupType;

@Repository
public interface LookUpTypeDAO extends JpaRepository<LookupType, Long> {

	Optional<LookupType> findByLookupType(String type);

	
}
