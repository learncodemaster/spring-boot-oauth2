package com.ults.IPMS.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ults.IPMS.util.MyEntityListener;

import io.swagger.annotations.ApiModelProperty;
@MappedSuperclass
@EntityListeners(MyEntityListener.class)
public class CommonFields implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@CreationTimestamp
	@JsonProperty(value = "created_time", access = Access.WRITE_ONLY)
	@Column( insertable = true, updatable = false)
	@ApiModelProperty(hidden=true)
	private Timestamp createdTime;
	
	@UpdateTimestamp
	@Column( insertable = false, updatable = true)
	@ApiModelProperty(hidden=true)
	@JsonProperty(value = "modified_time", access = Access.WRITE_ONLY)
	private Timestamp modifiedTime;
	
	@JsonProperty(value = "created_by", access = Access.WRITE_ONLY)
	@CreatedBy
	@Column( insertable = true, updatable = false)
	@ApiModelProperty(hidden=true)
	private String  createdBy;
	
	@JsonProperty(value = "modified_by", access = Access.WRITE_ONLY)
	@LastModifiedBy
	@Column( insertable = false, updatable = true)
	@ApiModelProperty(hidden=true)
	private String modifiedBy;
	
	
	@JsonProperty(value = "is_active", access = Access.WRITE_ONLY)
	@Column(insertable =  true,updatable = true)
	@ApiModelProperty(hidden = true)
	private Boolean is_active=true;

	public Timestamp getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public Timestamp getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}



	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Boolean getIs_active() {
		return is_active;
	}

	public void setIs_active(Boolean is_active) {
		this.is_active = is_active;
	}
	
	
	

}
