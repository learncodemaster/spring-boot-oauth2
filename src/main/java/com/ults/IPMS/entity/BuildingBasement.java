package com.ults.IPMS.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="building_basement",schema = "ipms")
public class BuildingBasement extends CommonFields{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="building_id")
	@JsonBackReference
	private Building building;
	
	@Column(name="basement_num")
	private int basementNumber;
	
	@Column(name="basement_area")
	private int basementArea;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public int getBasementNumber() {
		return basementNumber;
	}

	public void setBasementNumber(int basementNumber) {
		this.basementNumber = basementNumber;
	}

	public int getBasementArea() {
		return basementArea;
	}

	public void setBasementArea(int basementArea) {
		this.basementArea = basementArea;
	}
	
	
	
	

}
