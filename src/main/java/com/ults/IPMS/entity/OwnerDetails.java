package com.ults.IPMS.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="owner_details",schema = "ipms")
public class OwnerDetails extends CommonFields implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="owner_name")
	private String ownerName;
	
	@Column(name="owner_occupation")
	private String ownerOccupation;
	
	@Column(name="house_name")
	private String houseName;
	
	@Column(name="house_num")
	private String houseNum;
	
	@Column(name="street_name")
	private String StreetName;
	
	@Column(name="post_office")
	private String postOffice;
	
	@Column(name="pincode")
	private int pincode;
	
	@Column(name="email")
	private String email;
	
	@Column(name="landline_no")
	private String landlineNo;
	
	@Column(name="mobile_no")
	private String mobileNo;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinColumn(name="project_id")
	private Project project;
	
	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,mappedBy = "owner")
	@JsonManagedReference
	private Set<OwnerBuildingProperty> ownerBuildingProperty=new HashSet<OwnerBuildingProperty>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerOccupation() {
		return ownerOccupation;
	}

	public void setOwnerOccupation(String ownerOccupation) {
		this.ownerOccupation = ownerOccupation;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getHouseNum() {
		return houseNum;
	}

	public void setHouseNum(String houseNum) {
		this.houseNum = houseNum;
	}

	public String getStreetName() {
		return StreetName;
	}

	public void setStreetName(String streetName) {
		StreetName = streetName;
	}

	public String getPostOffice() {
		return postOffice;
	}

	public void setPostOffice(String postOffice) {
		this.postOffice = postOffice;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandlineNo() {
		return landlineNo;
	}

	public void setLandlineNo(String landlineNo) {
		this.landlineNo = landlineNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Set<OwnerBuildingProperty> getOwnerBuildingProperty() {
		return ownerBuildingProperty;
	}

	public void setOwnerBuildingProperty(Set<OwnerBuildingProperty> ownerBuildingProperty) {
		this.ownerBuildingProperty = ownerBuildingProperty;
	}


	

}
