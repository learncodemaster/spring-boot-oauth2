package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="additional_details",schema = "ipms")
public class AdditionalDetails extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	@Column(name="plot_area")
	private String plotArea;
	
	@Column(name="has_four_wheeler")
	private boolean hasFourWheeler;
	
	@Column(name="has_well_perenial_details")
	private boolean hasWellPerenialDetails;
	
	@Column(name="perenial_month")
	private String perenialMonth;
	
	@Column(name="cultivated_land")
	private boolean cultivatedLand;
	
	@Column(name="uncultivated")
	private boolean uncultivated;
	
	@Column(name="thozhilurapp")
	private boolean thozhilurapp;
	
	@Column(name="kudumbasree")
	private boolean kudumbasree;
	
	
	@Column(name="health_insurance")
	private boolean healthInsurance;
	
	@Column(name="property_financial_support")
	private boolean propertyFinancialSupport;
	
	@Column(name="waste_mngmt")
	private boolean wasteMngmt;
	
	@Column(name="remarks")
	private String remarks;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public String getPlotArea() {
		return plotArea;
	}

	public void setPlotArea(String plotArea) {
		this.plotArea = plotArea;
	}

	public boolean isHasFourWheeler() {
		return hasFourWheeler;
	}

	public void setHasFourWheeler(boolean hasFourWheeler) {
		this.hasFourWheeler = hasFourWheeler;
	}

	public boolean isHasWellPerenialDetails() {
		return hasWellPerenialDetails;
	}

	public void setHasWellPerenialDetails(boolean hasWellPerenialDetails) {
		this.hasWellPerenialDetails = hasWellPerenialDetails;
	}

	public String getPerenialMonth() {
		return perenialMonth;
	}

	public void setPerenialMonth(String perenialMonth) {
		this.perenialMonth = perenialMonth;
	}

	public boolean isCultivatedLand() {
		return cultivatedLand;
	}

	public void setCultivatedLand(boolean cultivatedLand) {
		this.cultivatedLand = cultivatedLand;
	}

	public boolean isUncultivated() {
		return uncultivated;
	}

	public void setUncultivated(boolean uncultivated) {
		this.uncultivated = uncultivated;
	}

	public boolean isThozhilurapp() {
		return thozhilurapp;
	}

	public void setThozhilurapp(boolean thozhilurapp) {
		this.thozhilurapp = thozhilurapp;
	}

	public boolean isKudumbasree() {
		return kudumbasree;
	}

	public void setKudumbasree(boolean kudumbasree) {
		this.kudumbasree = kudumbasree;
	}

	public boolean isHealthInsurance() {
		return healthInsurance;
	}

	public void setHealthInsurance(boolean healthInsurance) {
		this.healthInsurance = healthInsurance;
	}

	public boolean isPropertyFinancialSupport() {
		return propertyFinancialSupport;
	}

	public void setPropertyFinancialSupport(boolean propertyFinancialSupport) {
		this.propertyFinancialSupport = propertyFinancialSupport;
	}

	public boolean isWasteMngmt() {
		return wasteMngmt;
	}

	public void setWasteMngmt(boolean wasteMngmt) {
		this.wasteMngmt = wasteMngmt;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
