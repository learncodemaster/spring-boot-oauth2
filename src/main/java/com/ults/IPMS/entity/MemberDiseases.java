package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="member_diseases",schema = "ipms")
public class MemberDiseases extends CommonFields implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="member_id")
	private MemberDetails member;
	
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "desease_id")
	private Lookup desease;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public MemberDetails getMember() {
		return member;
	}


	public void setMember(MemberDetails member) {
		this.member = member;
	}


	public Lookup getDesease() {
		return desease;
	}


	public void setDesease(Lookup desease) {
		this.desease = desease;
	}
}
