package com.ults.IPMS.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="lookup_type")
public class LookupType extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="lookup_type")
	private String  lookupType;
	
	@Column(name="display_value")
	private String displayValue;
	
	@Column(name="description")
	private String description;
	
	@OneToMany(mappedBy="lookupType")
	@JsonManagedReference
	@JsonInclude(Include.NON_EMPTY)
	private Set<Lookup> lookups = new HashSet<Lookup>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLookupType() {
		return lookupType;
	}

	public void setLookupType(String lookupType) {
		this.lookupType = lookupType;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Lookup> getLookups() {
		return lookups;
	}

	public void setLookups(Set<Lookup> lookups) {
		this.lookups = lookups;
	}
	

	
}
