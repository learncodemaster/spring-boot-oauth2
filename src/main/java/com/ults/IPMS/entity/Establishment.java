package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "establishment",schema = "ipms")
public class Establishment extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="est_type_id")
	private Lookup establishmentType;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	@Column(name="est_name")
	private String estName;
	
	@Column(name="est_year")
	private String estYear;
	
	@Column(name="est_in_charge")
	private String estInCharge;
	
	@Column(name="in_charge_role")
	private String inChargeRole;
	
	@Column (name="emp_count")
	private String empCount;
	
	
	@Column(name="license_available")
	private boolean licenseAvailable;
	
	@Column(name="license_num")
	private String licenseNum;
	
	@Column(name="email")
	private String email;
	
	@Column(name="landline_no")
	private String landlineNo;
	
	@Column(name="mobile_no")
	private String mobileNo;
	
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Lookup getEstablishmentType() {
		return establishmentType;
	}

	public void setEstablishmentType(Lookup establishmentType) {
		this.establishmentType = establishmentType;
	}

	public String getEstName() {
		return estName;
	}

	public void setEstName(String estName) {
		this.estName = estName;
	}

	public String getEstYear() {
		return estYear;
	}

	public void setEstYear(String estYear) {
		this.estYear = estYear;
	}

	public String getEstInCharge() {
		return estInCharge;
	}

	public void setEstInCharge(String estInCharge) {
		this.estInCharge = estInCharge;
	}

	public String getInChargeRole() {
		return inChargeRole;
	}

	public void setInChargeRole(String inChargeRole) {
		this.inChargeRole = inChargeRole;
	}

	public String getEmpCount() {
		return empCount;
	}

	public void setEmpCount(String empCount) {
		this.empCount = empCount;
	}

	public boolean isLicenseAvailable() {
		return licenseAvailable;
	}

	public void setLicenseAvailable(boolean licenseAvailable) {
		this.licenseAvailable = licenseAvailable;
	}

	public String getLicenseNum() {
		return licenseNum;
	}

	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandlineNo() {
		return landlineNo;
	}

	public void setLandlineNo(String landlineNo) {
		this.landlineNo = landlineNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	
	

}
