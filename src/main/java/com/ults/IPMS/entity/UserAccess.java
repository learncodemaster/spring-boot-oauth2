package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="user_access")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAccess extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name="first_Name")
	//@JsonProperty("first_Name")
	private String firstName;

	//@JsonProperty("last_name")
	@Column(name="last_name")
	private String lastName;

	//@JsonProperty("username")
	@Column(name="username")
	private String username;

	//@JsonProperty("email")
	@Column(name="email")
	private String email;

	//@JsonProperty("contact_no")
	@Column(name="contact_no")
	private String contactNo;


	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;

	@OneToOne(mappedBy = "userAccess",cascade = CascadeType.ALL)
	@JsonManagedReference 
	private Login login;


	//@JsonProperty("status")
	@Column(name="status")
	private String status;



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLast_name() {
		return lastName;
	}

	public void setLast_name(String last_name) {
		this.lastName = last_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact_no() {
		return contactNo;
	}

	public void setContact_no(String contact_no) {
		this.contactNo = contact_no;
	}


	public Role getRole() { 
		return role; }

	public void setRole(Role role) { 
		this.role = role; }

	public Login getLogin() { 
		return login; }

	public void setLogin(Login login) { 
		this.login = login; }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
  

}
