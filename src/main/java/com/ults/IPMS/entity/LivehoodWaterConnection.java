package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Livelihood_water_connection",schema = "ipms")
public class LivehoodWaterConnection extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "livelihood_id")
	private LivelihoodDetails livelihoodDetails;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "water_source_type_id")
	private Lookup waterSource;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LivelihoodDetails getLivelihoodDetails() {
		return livelihoodDetails;
	}

	public void setLivelihoodDetails(LivelihoodDetails livelihoodDetails) {
		this.livelihoodDetails = livelihoodDetails;
	}

	public Lookup getWaterSource() {
		return waterSource;
	}

	public void setWaterSource(Lookup waterSource) {
		this.waterSource = waterSource;
	}
	
	
	
	

}
