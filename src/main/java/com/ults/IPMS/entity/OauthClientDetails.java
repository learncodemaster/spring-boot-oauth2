package com.ults.IPMS.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="oauth_client_details")
public class OauthClientDetails {
	
	
	@Id
	@Column(name="client_id")
	@JsonProperty("client_id")
	private String clientID;
	
	
	@Column(name="resource_ids")
	@JsonProperty("resource_ids")
	private String resourceIds;
	
	@Column(name="client_secret")
	@JsonProperty("client_secret")
	private String clientSecret;
	
	@Column(name="scope")
	@JsonProperty("scope")
	private String scope;
	
	@Column(name="authorized_grant_types")
	@JsonProperty("authorized_grant_types")
	private String authorizedGrantTypes;
	
	@Column(name="web_server_redirect_uri")
	@JsonProperty("web_server_redirect_uri")
	private String webServerRedirectURI;
	
	
	@Column(name="authorities")
	@JsonProperty("authorities")
	private String authorities;
	
	@Column(name="access_token_validity")
	@JsonProperty("access_token_validity")
	private int accessTokenValidity;
	
	@Column(name="refresh_token_validity")
	@JsonProperty("refresh_token_validity")
	private int refreshTokenValidity;

	@Column(name="additional_information")
	@JsonProperty("additional_information")
	private String additionalInformation;
	
	@Column(name="autoapprove")
	@JsonProperty("autoapprove")
	private String autoApprove;

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	public String getWebServerRedirectURI() {
		return webServerRedirectURI;
	}

	public void setWebServerRedirectURI(String webServerRedirectURI) {
		this.webServerRedirectURI = webServerRedirectURI;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public int getAccessTokenValidity() {
		return accessTokenValidity;
	}

	public void setAccessTokenValidity(int accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

	public int getRefreshTokenValidity() {
		return refreshTokenValidity;
	}

	public void setRefreshTokenValidity(int refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getAutoApprove() {
		return autoApprove;
	}

	public void setAutoApprove(String autoApprove) {
		this.autoApprove = autoApprove;
	}
	
	
	
}
