package com.ults.IPMS.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author shahbas
 *
 */
@Entity
@Table(name = "property_roof",schema = "ipms")
public class PropertyRoof extends CommonFields {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;

	@OneToOne
	@JoinColumn(name = "roof_type_id")
	private Lookup roofType;

	@Column(name = "roof_percent")
	private double roofPercent;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Lookup getRoofType() {
		return roofType;
	}

	public void setRoofType(Lookup roofType) {
		this.roofType = roofType;
	}

	public double getRoofPercent() {
		return roofPercent;
	}

	public void setRoofPercent(double roofPercent) {
		this.roofPercent = roofPercent;
	}




	
	
}
