package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Role_level")
public class RoleLevel extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "role_level")
	private String roleLevel;
	
	@Column(name = "role_level_description")
	private String roleLevelDescription;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole_level() {
		return roleLevel;
	}

	public void setRole_level(String role_level) {
		this.roleLevel = role_level;
	}

	public String getRole_level_description() {
		return roleLevelDescription;
	}

	public void setRole_level_description(String role_level_description) {
		this.roleLevelDescription = role_level_description;
	}

}
