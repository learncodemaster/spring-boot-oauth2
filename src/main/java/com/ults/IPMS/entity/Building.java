package com.ults.IPMS.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="building",schema = "ipms")
public class Building extends CommonFields{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	@Column(name="floor_count",nullable = false)
	private int floorCount;
	
	@Column(name="property_count")
	private int propertyCount;
	
	@Column(name="basement_available")
	private boolean basementAvailable;
	
	
	@Column(name="basement_count")
	private int basementCount;
	
	@Column(name="is_landmark")
	private boolean isLandmark;
	
	@Column(name="landmark_id")
	private int landmarkId;
	
	@Column(name="building_name")
	private String buildingName;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "location_id")
	private Location location;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="building_zone")
	private Lookup buildingZone;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "building")
	@JsonManagedReference
	private Set<BuildingBasement> buildingBasement = new HashSet<BuildingBasement>();
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "building")
	@JsonManagedReference
	private Set<BuildingFloor> buildingFloor= new HashSet<BuildingFloor>();
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "building")
	@JsonManagedReference
	private Set<OwnerBuildingProperty>ownerBuildingProperty= new HashSet<OwnerBuildingProperty>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getFloorCount() {
		return floorCount;
	}

	public void setFloorCount(int floorCount) {
		this.floorCount = floorCount;
	}

	public int getPropertyCount() {
		return propertyCount;
	}

	public void setPropertyCount(int propertyCount) {
		this.propertyCount = propertyCount;
	}

	public boolean isBasementAvailable() {
		return basementAvailable;
	}

	public void setBasementAvailable(boolean basementAvailable) {
		this.basementAvailable = basementAvailable;
	}

	public int getBasementCount() {
		return basementCount;
	}

	public void setBasementCount(int basementCount) {
		this.basementCount = basementCount;
	}

	public boolean isLandmark() {
		return isLandmark;
	}

	public void setLandmark(boolean isLandmark) {
		this.isLandmark = isLandmark;
	}

	public int getLandmarkId() {
		return landmarkId;
	}

	public void setLandmarkId(int landmarkId) {
		this.landmarkId = landmarkId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Set<BuildingBasement> getBuildingBasement() {
		return buildingBasement;
	}

	public void setBuildingBasement(Set<BuildingBasement> buildingBasement) {
		this.buildingBasement = buildingBasement;
	}

	public Set<BuildingFloor> getBuildingFloor() {
		return buildingFloor;
	}

	public void setBuildingFloor(Set<BuildingFloor> buildingFloor) {
		this.buildingFloor = buildingFloor;
	}

	public Set<OwnerBuildingProperty> getOwnerBuildingProperty() {
		return ownerBuildingProperty;
	}

	public void setOwnerBuildingProperty(Set<OwnerBuildingProperty> ownerBuildingProperty) {
		this.ownerBuildingProperty = ownerBuildingProperty;
	}

	public Lookup getBuildingZone() {
		return buildingZone;
	}

	public void setBuildingZone(Lookup buildingZone) {
		this.buildingZone = buildingZone;
	}


	

}
