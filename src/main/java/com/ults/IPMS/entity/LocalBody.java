package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LocalBody")
public class LocalBody extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(name="local_body_name")
	private String localBodyName;
	
	@ManyToOne
	@JoinColumn(name="taluk")
	private Taluk taluk;
	
	@ManyToOne
	@JoinColumn(name="local_body_type")
	private Lookup localBodyType;
	
	@Column(name="househole_count")
	private String householdCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocalBodyName() {
		return localBodyName;
	}

	public void setLocalBodyName(String localBodyName) {
		this.localBodyName = localBodyName;
	}

	public Taluk getTaluk() {
		return taluk;
	}

	public void setTaluk(Taluk taluk) {
		this.taluk = taluk;
	}

	public Lookup getLocalBodyType() {
		return localBodyType;
	}

	public void setLocalBodyType(Lookup localBodyType) {
		this.localBodyType = localBodyType;
	}

	public String getHouseholdCount() {
		return householdCount;
	}

	public void setHouseholdCount(String householdCount) {
		this.householdCount = householdCount;
	}
	
	
	
	
	
}
