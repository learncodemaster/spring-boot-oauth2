package com.ults.IPMS.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "property",schema = "ipms")
public class Property extends CommonFields{

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "building_id")
	private Building building;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "property_status_id")
	private Lookup propertyStatus;
	
	@Column(name="new_id")
	private int newId;
	
	@Column(name="old_id")
	private int oldId;
	
	@Column(name="uid",unique = true)
	private String uid;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="owner_name")
	private String ownerName;
	
	@Column(name = "photo1")
	private String photo1;
	
	@Column(name = "photo2")
	private String photo2;
	
	@Column(name = "informed_by")
	private String informedBy;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "door_status_id")
	private Lookup doorStatus;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "property_sector_id")
	private Lookup propertySector;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "property_type_id")
	private Lookup propertyType;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "property_subtype_id")
	private Lookup propertySubType;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "property_usage_type_id")
	private Lookup propertyUsageType;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="building_under_id")
	private Lookup buildingUnder;
	
	@Column(name="electrical_connection")
	private boolean electricalConnection;
	
	@Column(name = "consumer_number")
	private String consumerNumber;
	
	@Column(name = "latrine_available")
	private boolean latrineAvailable;
	
	@Column(name = "ac_available")
	private boolean acAvailable;
	
	@Column(name = "is_landmark")
	private boolean isLandmark;
	
	@OneToOne
	@JoinColumn(name = "landmark_id")
	private LandMark landmark;

	@Column(name = "survey_num")
	private int surveyNumber;
	
	@Column(name = "year_of_construction")
	private int yearOfConstruction;
	
	@Column(name = "years_count")
	private int yearsCount;
	
	@Column(name = "room_count")
	private int roomCount;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "structure_type_id")
	private Lookup structureType;
	
	@Column(name = "carporch_available")
	private boolean carporchAvailbale;
	
	@Column(name = "carporch_area")
	private int carporchArea;
	
	@Column(name="common_staircase_available")
	private boolean commonStairCaseAvailbale;
	
	@Column(name = "common_staircase_area")
	private int commonStairCaseArea;
	
	@Column(name = "higher_floor_type_gt_250")
	private boolean higherFloorTypeGT250;
	
	@Column(name = "wall_type_id")
	private int wallTypeId;
	
	@OneToMany
	@JoinColumn(name="floor_type")
	private Set<Lookup> floorType;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<AdditionalDetails> additionalDetails=new HashSet<AdditionalDetails>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<PropertyFloor> propertyFloor=new HashSet<PropertyFloor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<PropertyRoad> propertyRoad=new HashSet<PropertyRoad>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<PropertyRoof> propertyRoof=new HashSet<PropertyRoof>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<PropertyTax> propertyTax=new HashSet<PropertyTax>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<PropertyWaste> propertyWaste=new HashSet<PropertyWaste>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<Establishment> establishment=new HashSet<Establishment>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<Tenant> tenant=new HashSet<Tenant>();
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private LivelihoodDetails livelihoodDetail=new LivelihoodDetails();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<MemberDetails> memberDetail=new HashSet<MemberDetails>();
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "property")
	@JsonManagedReference
	private Set<OwnerBuildingProperty> ownerBuildingProperty =new HashSet<OwnerBuildingProperty>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "property_owner", joinColumns = 
	@JoinColumn(name = "property_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
	private Set<OwnerDetails> ownerDetails = new HashSet<OwnerDetails>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public Lookup getPropertyStatus() {
		return propertyStatus;
	}

	public void setPropertyStatus(Lookup propertyStatus) {
		this.propertyStatus = propertyStatus;
	}

	public int getNewId() {
		return newId;
	}

	public void setNewId(int newId) {
		this.newId = newId;
	}

	public int getOldId() {
		return oldId;
	}

	public void setOldId(int oldId) {
		this.oldId = oldId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getPhoto1() {
		return photo1;
	}

	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}

	public String getPhoto2() {
		return photo2;
	}

	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}

	public String getInformedBy() {
		return informedBy;
	}

	public void setInformedBy(String informedBy) {
		this.informedBy = informedBy;
	}

	public Lookup getDoorStatus() {
		return doorStatus;
	}

	public void setDoorStatus(Lookup doorStatus) {
		this.doorStatus = doorStatus;
	}

	public Lookup getPropertySector() {
		return propertySector;
	}

	public void setPropertySector(Lookup propertySector) {
		this.propertySector = propertySector;
	}

	public Lookup getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(Lookup propertyType) {
		this.propertyType = propertyType;
	}

	public Lookup getPropertySubType() {
		return propertySubType;
	}

	public void setPropertySubType(Lookup propertySubType) {
		this.propertySubType = propertySubType;
	}

	public Lookup getPropertyUsageType() {
		return propertyUsageType;
	}

	public void setPropertyUsageType(Lookup propertyUsageType) {
		this.propertyUsageType = propertyUsageType;
	}

	public boolean isElectricalConnection() {
		return electricalConnection;
	}

	public void setElectricalConnection(boolean electricalConnection) {
		this.electricalConnection = electricalConnection;
	}

	public String getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(String consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public boolean isLatrineAvailable() {
		return latrineAvailable;
	}

	public void setLatrineAvailable(boolean latrineAvailable) {
		this.latrineAvailable = latrineAvailable;
	}

	public boolean isAcAvailable() {
		return acAvailable;
	}

	public void setAcAvailable(boolean acAvailable) {
		this.acAvailable = acAvailable;
	}

	public boolean isLandmark() {
		return isLandmark;
	}

	public void setLandmark(boolean isLandmark) {
		this.isLandmark = isLandmark;
	}



	public LandMark getLandmark() {
		return landmark;
	}

	public void setLandmark(LandMark landmark) {
		this.landmark = landmark;
	}

	public int getSurveyNumber() {
		return surveyNumber;
	}

	public void setSurveyNumber(int surveyNumber) {
		this.surveyNumber = surveyNumber;
	}

	public int getYearOfConstruction() {
		return yearOfConstruction;
	}

	public void setYearOfConstruction(int yearOfConstruction) {
		this.yearOfConstruction = yearOfConstruction;
	}

	public int getYearsCount() {
		return yearsCount;
	}

	public void setYearsCount(int yearsCount) {
		this.yearsCount = yearsCount;
	}

	public int getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(int roomCount) {
		this.roomCount = roomCount;
	}

	public Lookup getStructureType() {
		return structureType;
	}

	public void setStructureType(Lookup structureType) {
		this.structureType = structureType;
	}

	public boolean isCarporchAvailbale() {
		return carporchAvailbale;
	}

	public void setCarporchAvailbale(boolean carporchAvailbale) {
		this.carporchAvailbale = carporchAvailbale;
	}

	public int getCarporchArea() {
		return carporchArea;
	}

	public void setCarporchArea(int carporchArea) {
		this.carporchArea = carporchArea;
	}

	public boolean isCommonStairCaseAvailbale() {
		return commonStairCaseAvailbale;
	}

	public void setCommonStairCaseAvailbale(boolean commonStairCaseAvailbale) {
		this.commonStairCaseAvailbale = commonStairCaseAvailbale;
	}

	public int getCommonStairCaseArea() {
		return commonStairCaseArea;
	}

	public void setCommonStairCaseArea(int commonStairCaseArea) {
		this.commonStairCaseArea = commonStairCaseArea;
	}

	public boolean isHigherFloorTypeGT250() {
		return higherFloorTypeGT250;
	}

	public void setHigherFloorTypeGT250(boolean higherFloorTypeGT250) {
		this.higherFloorTypeGT250 = higherFloorTypeGT250;
	}

	public int getWallTypeId() {
		return wallTypeId;
	}

	public void setWallTypeId(int wallTypeId) {
		this.wallTypeId = wallTypeId;
	}

	public Set<AdditionalDetails> getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(Set<AdditionalDetails> additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public Set<PropertyFloor> getPropertyFloor() {
		return propertyFloor;
	}

	public void setPropertyFloor(Set<PropertyFloor> propertyFloor) {
		this.propertyFloor = propertyFloor;
	}

	public Set<PropertyRoad> getPropertyRoad() {
		return propertyRoad;
	}

	public void setPropertyRoad(Set<PropertyRoad> propertyRoad) {
		this.propertyRoad = propertyRoad;
	}

	public Set<PropertyRoof> getPropertyRoof() {
		return propertyRoof;
	}

	public void setPropertyRoof(Set<PropertyRoof> propertyRoof) {
		this.propertyRoof = propertyRoof;
	}

	public Set<PropertyTax> getPropertyTax() {
		return propertyTax;
	}

	public void setPropertyTax(Set<PropertyTax> propertyTax) {
		this.propertyTax = propertyTax;
	}

	public Set<PropertyWaste> getPropertyWaste() {
		return propertyWaste;
	}

	public void setPropertyWaste(Set<PropertyWaste> propertyWaste) {
		this.propertyWaste = propertyWaste;
	}

	public Set<Establishment> getEstablishment() {
		return establishment;
	}

	public void setEstablishment(Set<Establishment> establishment) {
		this.establishment = establishment;
	}

	public Set<Tenant> getTenant() {
		return tenant;
	}

	public void setTenant(Set<Tenant> tenant) {
		this.tenant = tenant;
	}

	public LivelihoodDetails getLivelihoodDetail() {
		return livelihoodDetail;
	}

	public void setLivelihoodDetail(LivelihoodDetails livelihoodDetail) {
		this.livelihoodDetail = livelihoodDetail;
	}

	public Set<MemberDetails> getMemberDetail() {
		return memberDetail;
	}

	public void setMemberDetail(Set<MemberDetails> memberDetail) {
		this.memberDetail = memberDetail;
	}

	public Set<OwnerBuildingProperty> getOwnerBuildingProperty() {
		return ownerBuildingProperty;
	}

	public void setOwnerBuildingProperty(Set<OwnerBuildingProperty> ownerBuildingProperty) {
		this.ownerBuildingProperty = ownerBuildingProperty;
	}

	public Lookup getBuildingUnder() {
		return buildingUnder;
	}

	public void setBuildingUnder(Lookup buildingUnder) {
		this.buildingUnder = buildingUnder;
	}

	public Set<Lookup> getFloorType() {
		return floorType;
	}

	public void setFloorType(Set<Lookup> floorType) {
		this.floorType = floorType;
	}

	public Set<OwnerDetails> getOwnerDetails() {
		return ownerDetails;
	}

	public void setOwnerDetails(Set<OwnerDetails> ownerDetails) {
		this.ownerDetails = ownerDetails;
	}


	
}
