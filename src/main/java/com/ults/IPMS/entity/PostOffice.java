package com.ults.IPMS.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class PostOffice extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="post_office")
	private String postoffice;
	
	@Column(name="pin_no")
	private int pin;
	
	@OneToMany
	@JoinColumn(name="local_body")
	private Set<LocalBody> localBody=new HashSet<LocalBody>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPostoffice() {
		return postoffice;
	}

	public void setPostoffice(String postoffice) {
		this.postoffice = postoffice;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public Set<LocalBody> getLocalBody() {
		return localBody;
	}

	public void setLocalBody(Set<LocalBody> localBody) {
		this.localBody = localBody;
	}

	
	
}
