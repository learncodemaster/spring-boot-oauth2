package com.ults.IPMS.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "property_waste",schema = "ipms")
public class PropertyWaste extends CommonFields {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	
	@OneToOne
	@JoinColumn(name = "water_mngmt_type_id")
	private Lookup waterManagementType;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Property getProperty() {
		return property;
	}


	public void setProperty(Property property) {
		this.property = property;
	}


	public Lookup getWaterManagementType() {
		return waterManagementType;
	}


	public void setWaterManagementType(Lookup waterManagementType) {
		this.waterManagementType = waterManagementType;
	}
	
	
}
