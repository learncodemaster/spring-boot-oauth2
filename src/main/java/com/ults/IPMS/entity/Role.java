package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Role extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="role_name")
	private String roleName;
	
//	@ManyToOne
//	private UserCreation userCreation;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="role_level_id")
	private RoleLevel roleLovel;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole_name() {
		return roleName;
	}
	public void setRole_name(String role_name) {
		this.roleName = role_name;
	}
	public RoleLevel getRoleLovel() {
		return roleLovel;
	}
	public void setRoleLovel(RoleLevel roleLovel) {
		this.roleLovel = roleLovel;
	}


}
