package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="owner_building_property")
public class OwnerBuildingProperty extends CommonFields implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name = "ownerId")
	@JsonBackReference
	private OwnerDetails owner;

	@ManyToOne
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;

	@ManyToOne
	@JoinColumn(name = "building_id")
	@JsonBackReference
	private Building building;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public OwnerDetails getOwner() {
		return owner;
	}

	public void setOwner(OwnerDetails owner) {
		this.owner = owner;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

}
