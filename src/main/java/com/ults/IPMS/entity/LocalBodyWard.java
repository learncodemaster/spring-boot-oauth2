package com.ults.IPMS.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class LocalBodyWard extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="ward_name")
	private String wardName;
	
	@Column(name="ward_no")
	private int wardNo;
	
	@OneToMany
	@JoinColumn(name="local_body")
	private  Set<LocalBody> localBody=new HashSet<LocalBody>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public int getWardNo() {
		return wardNo;
	}

	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}

	public Set<LocalBody> getLocalBody() {
		return localBody;
	}

	public void setLocalBody(Set<LocalBody> localBody) {
		this.localBody = localBody;
	}


	
	

}
