package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="member_pension",schema = "ipms")
public class MemberPension extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="member_id")
	private MemberDetails member;
	
	@OneToOne
	@JoinColumn(name="pension_id")
	private Lookup pension;

	public MemberDetails getMember() {
		return member;
	}

	public void setMember(MemberDetails member) {
		this.member = member;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Lookup getPension() {
		return pension;
	}

	public void setPension(Lookup pension) {
		this.pension = pension;
	}


	
	
	

}
