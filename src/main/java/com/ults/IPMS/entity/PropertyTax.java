package com.ults.IPMS.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "property_tax",schema = "ipms")
public class PropertyTax extends CommonFields{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	@Column(name = "tax_receipt_num")
	private String taxReceiptNumber;
	
	@Column(name = "tax_amount")
	private double taxAmount;
	
	@Column(name = "tax_paid_date")
	private Date taxPaidDate;
	
	@Column(name="tax_paid_year")
	private int taxPaidYear;
	
	@Column(name = "annual_tax_amount")
	private double annualTaxAmount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public String getTaxReceiptNumber() {
		return taxReceiptNumber;
	}

	public void setTaxReceiptNumber(String taxReceiptNumber) {
		this.taxReceiptNumber = taxReceiptNumber;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Date getTaxPaidDate() {
		return taxPaidDate;
	}

	public void setTaxPaidDate(Date taxPaidDate) {
		this.taxPaidDate = taxPaidDate;
	}

	public int getTaxPaidYear() {
		return taxPaidYear;
	}

	public void setTaxPaidYear(int taxPaidYear) {
		this.taxPaidYear = taxPaidYear;
	}

	public double getAnnualTaxAmount() {
		return annualTaxAmount;
	}

	public void setAnnualTaxAmount(double annualTaxAmount) {
		this.annualTaxAmount = annualTaxAmount;
	}
	
	

}
