package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="Livelihood",schema = "ipms")
public class LivelihoodDetails extends CommonFields implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
 
	@ManyToOne
	@JoinColumn(name="member_id")
	private MemberDetails member;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	@Column(name="member_count")
	private int memberCount;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "relegion_id")
	private Lookup religion;
	
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cast_id")
	private Lookup cast;

	@Column(name="ration_card_available")
	private boolean raionCardAvailable;
	
	@Column(name="ration_card_num")
	private String rationCardNum;
	
	@Column(name="bathroom_available")
	private boolean bathroomAvailable;
	
	@Column(name="water_conn_available")
	private boolean waterConnAvailable;
	
	@Column(name="gas_conn_available")
	private boolean gasConnAvailable;
	
	@Column(name="rain_water_harvesting")
	private  boolean rainWaterHarvesting;
	
	@Column(name="waste_mngmt")
	private boolean wastemngmt;
	
	@Column(name="bank_acc_available")
	private boolean bankAccavailable;
	
	@Column(name="has_pet_dog")
	private boolean hasPetDog;
	
	@Column(name="has_cattle")
	private boolean hasCattle;
	
	@Column(name="cattle_count")
	private int cattkeCount;
	
	@Column(name="has_poultry")
	private boolean hasPoultry;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MemberDetails getMember() {
		return member;
	}

	public void setMember(MemberDetails member) {
		this.member = member;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public boolean isRaionCardAvailable() {
		return raionCardAvailable;
	}

	public void setRaionCardAvailable(boolean raionCardAvailable) {
		this.raionCardAvailable = raionCardAvailable;
	}

	public String getRationCardNum() {
		return rationCardNum;
	}

	public void setRationCardNum(String rationCardNum) {
		this.rationCardNum = rationCardNum;
	}

	public boolean isBathroomAvailable() {
		return bathroomAvailable;
	}

	public void setBathroomAvailable(boolean bathroomAvailable) {
		this.bathroomAvailable = bathroomAvailable;
	}

	public boolean isWaterConnAvailable() {
		return waterConnAvailable;
	}

	public void setWaterConnAvailable(boolean waterConnAvailable) {
		this.waterConnAvailable = waterConnAvailable;
	}

	public boolean isGasConnAvailable() {
		return gasConnAvailable;
	}

	public void setGasConnAvailable(boolean gasConnAvailable) {
		this.gasConnAvailable = gasConnAvailable;
	}

	public boolean isRainWaterHarvesting() {
		return rainWaterHarvesting;
	}

	public void setRainWaterHarvesting(boolean rainWaterHarvesting) {
		this.rainWaterHarvesting = rainWaterHarvesting;
	}

	public boolean isWastemngmt() {
		return wastemngmt;
	}

	public void setWastemngmt(boolean wastemngmt) {
		this.wastemngmt = wastemngmt;
	}

	public boolean isBankAccavailable() {
		return bankAccavailable;
	}

	public void setBankAccavailable(boolean bankAccavailable) {
		this.bankAccavailable = bankAccavailable;
	}

	public boolean isHasPetDog() {
		return hasPetDog;
	}

	public void setHasPetDog(boolean hasPetDog) {
		this.hasPetDog = hasPetDog;
	}

	public boolean isHasCattle() {
		return hasCattle;
	}

	public void setHasCattle(boolean hasCattle) {
		this.hasCattle = hasCattle;
	}

	public int getCattkeCount() {
		return cattkeCount;
	}

	public void setCattkeCount(int cattkeCount) {
		this.cattkeCount = cattkeCount;
	}

	public boolean isHasPoultry() {
		return hasPoultry;
	}

	public void setHasPoultry(boolean hasPoultry) {
		this.hasPoultry = hasPoultry;
	}

	public Lookup getReligion() {
		return religion;
	}

	public void setReligion(Lookup religion) {
		this.religion = religion;
	}

	public Lookup getCast() {
		return cast;
	}

	public void setCast(Lookup cast) {
		this.cast = cast;
	}
	

}
