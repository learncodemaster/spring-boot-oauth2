package com.ults.IPMS.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="member_details",schema = "ipms")
public class MemberDetails extends CommonFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	
	@Column(name="name")
	private String name;
	
	@Column(name="age")
	private int age;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="marital_status")
	private String maritalStatus;
	
	@OneToOne
	@JoinColumn(name="educ_id")
	private Lookup education;
	
	@OneToOne
	@JoinColumn(name="educ_sub_id")
	private Lookup educationSub;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "job_id")
	private Lookup jobDetails;
		
	@Column(name="job")
	private String job;
	
	@Column(name="has_disability")
	private boolean hasDisability;
	
	@Column(name="disability_percent")
	private String disabilityPercent;
	
	@Column(name="has_diseases")
	private boolean hasDiseases;
	
	@Column(name="has_pension")
	private boolean hasPension;
	
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Lookup getEducation() {
		return education;
	}

	public void setEducation(Lookup education) {
		this.education = education;
	}

	public Lookup getEducationSub() {
		return educationSub;
	}

	public void setEducationSub(Lookup educationSub) {
		this.educationSub = educationSub;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public boolean isHasDisability() {
		return hasDisability;
	}

	public void setHasDisability(boolean hasDisability) {
		this.hasDisability = hasDisability;
	}

	public String getDisabilityPercent() {
		return disabilityPercent;
	}

	public void setDisabilityPercent(String disabilityPercent) {
		this.disabilityPercent = disabilityPercent;
	}

	public boolean isHasDiseases() {
		return hasDiseases;
	}

	public void setHasDiseases(boolean hasDiseases) {
		this.hasDiseases = hasDiseases;
	}

	public boolean isHasPension() {
		return hasPension;
	}

	public void setHasPension(boolean hasPension) {
		this.hasPension = hasPension;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Lookup getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(Lookup jobDetails) {
		this.jobDetails = jobDetails;
	}

	
	
}

