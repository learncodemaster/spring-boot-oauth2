package com.ults.IPMS.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "property_road",schema = "ipms")
public class PropertyRoad extends CommonFields{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	@JsonBackReference
	private Property property;
	
	@Column(name="near_road_name")
	private String nearRoadName;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "road_type_id")
	private Lookup roadType;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "road_width_id")
	private Lookup roadWidth;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public String getNearRoadName() {
		return nearRoadName;
	}

	public void setNearRoadName(String nearRoadName) {
		this.nearRoadName = nearRoadName;
	}

	public Lookup getRoadType() {
		return roadType;
	}

	public void setRoadType(Lookup roadType) {
		this.roadType = roadType;
	}

	public Lookup getRoadWidth() {
		return roadWidth;
	}

	public void setRoadWidth(Lookup roadWidth) {
		this.roadWidth = roadWidth;
	}
	
	

}
