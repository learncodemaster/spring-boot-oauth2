package com.ults.IPMS.service.Impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.OauthClientDetailsDAO;
import com.ults.IPMS.entity.OauthClientDetails;

@Service("clientDetailService")
public class ClientDetailsServiceImpl implements ClientDetailsService {

	@Autowired
	private OauthClientDetailsDAO oauthClientDetailsDAO;

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		OauthClientDetails oauthClientDetails = oauthClientDetailsDAO.findById(clientId)
				.orElseThrow(() -> new ClientRegistrationException(""));

		BaseClientDetails details = new BaseClientDetails();
		details.setClientId(oauthClientDetails.getClientID());
		details.setClientSecret(oauthClientDetails.getClientSecret());
		details.setAuthorizedGrantTypes(Arrays.asList(oauthClientDetails.getAuthorizedGrantTypes().split(",")));
		details.setScope(Arrays.asList(oauthClientDetails.getScope()));
		details.setAccessTokenValiditySeconds(oauthClientDetails.getAccessTokenValidity());
		details.setRefreshTokenValiditySeconds(oauthClientDetails.getRefreshTokenValidity());

		return details;
	}

}
