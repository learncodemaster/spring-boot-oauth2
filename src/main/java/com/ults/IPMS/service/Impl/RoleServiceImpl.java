package com.ults.IPMS.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.RoleDAO;
import com.ults.IPMS.dao.RoleLevelDAO;
import com.ults.IPMS.entity.Role;
import com.ults.IPMS.entity.RoleLevel;
import com.ults.IPMS.service.RoleService;
import com.ults.IPMS.util.CommonResponse;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleDAO roleDAO;

	@Autowired
	RoleLevelDAO roleLevelDAO;

	@Override
	public CommonResponse insert(Role role) {
		return new CommonResponse("Sucess", roleDAO.save(role));
	}

	@Override
	public CommonResponse insertRoleLevel(RoleLevel roleLevel) {
		return new CommonResponse("Sucess", roleLevelDAO.save(roleLevel));
	}

}
