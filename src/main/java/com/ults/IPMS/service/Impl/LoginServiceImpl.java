package com.ults.IPMS.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.LoginDAO;
import com.ults.IPMS.entity.Login;
import com.ults.IPMS.service.LoginService;
import com.ults.IPMS.util.CommonResponse;

@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginDAO logindao;
//	@Autowired
//	LoginHistoryDao historydao;

	@Override
	public CommonResponse login(Login login) { 
		// TODO Auto-generated method stub

		return new CommonResponse("Sucess", logindao.save(login));
	}

//	@Override
//	public CommonResponse history(LoginHistory loginHistory) {
//		// TODO Auto-generated method stub
//		if(!logindao.existsByUserName(loginHistory.getUserName())) {
//			return new CommonResponse("invalid username");
//		}
//		if(!logindao.existByPassword(loginHistory.getPassword())) {
//			return new CommonResponse("invalid password");
//		}
//		return new CommonResponse("success", historydao.save(loginHistory));
//	}  
}  
