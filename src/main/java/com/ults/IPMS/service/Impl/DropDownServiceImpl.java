package com.ults.IPMS.service.Impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.LookUpTypeDAO;
import com.ults.IPMS.dao.LookupDAO;
import com.ults.IPMS.entity.Lookup;
import com.ults.IPMS.entity.LookupType;
import com.ults.IPMS.model.ComboDTO;
import com.ults.IPMS.service.DropDownService;
import com.ults.IPMS.util.CommonResponse;

@Service
public class DropDownServiceImpl implements DropDownService {


	@Autowired
	LookupDAO lookUpDao;

	@Autowired
	LookUpTypeDAO lookUpTypeDao;

	@Override
	public CommonResponse insertLookup(Lookup lookup) {
		return new CommonResponse("sucess", lookUpDao.save(lookup), true);
	}

	@Override
	public CommonResponse insertLookupType(LookupType lookuptype) {
		return new CommonResponse("Sucess", lookUpTypeDao.save(lookuptype), true);
	}

	@Override
	public CommonResponse getLookup(long id) {
		return new CommonResponse("Success", lookUpDao.findById(id), true);
	}

	@Override
	public CommonResponse deleteLookup(long id) {
		lookUpDao.deleteById(id);
		return new CommonResponse("Success", true);
	}

	@Override
	public CommonResponse getType(long id) {
		return new CommonResponse("Success", lookUpTypeDao.findById(id), true);
	}

	@Override
	public CommonResponse deleteType(long id) {
		lookUpTypeDao.deleteById(id);
		return new CommonResponse("Success", true);
	}

	@Override
	public CommonResponse findAll(Pageable pageable) {
		return new CommonResponse("Success", lookUpDao.findAll(pageable), true);
	}

	@Override
	public CommonResponse typeFindAll(Pageable pageable) {
		return new CommonResponse("Success",lookUpTypeDao.findAll().stream().map(type->type.getLookupType()).collect(Collectors.toSet()), true);
	}

	@Override
	public CommonResponse listAllCombo(List<String> types) {
		Set<LookupType> lookupTypes = new HashSet<LookupType>();
		Map<String, Set<ComboDTO>> map = new HashMap<String, Set<ComboDTO>>();

		types.forEach(type -> {
			Optional<LookupType> lookupTypesObject = lookUpTypeDao.findByLookupType(type);
			if (lookupTypesObject.isPresent()) {
				lookupTypes.add(lookupTypesObject.get());
			}
		});

		lookupTypes.forEach(lt -> {
			Set<ComboDTO> lookups = new HashSet<ComboDTO>();
			for (Lookup item : lt.getLookups()) {
				if (lt.getId() == item.getLookupType().getId()) {
					lookups.add(new ComboDTO(item.getId(), item.getDisplayValue()));
					map.put(lt.getLookupType(), lookups);
				}
			}
		});
		return new CommonResponse("Success", map);
	}

}
