package com.ults.IPMS.service.Impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.LoginDAO;
import com.ults.IPMS.dao.UserAccessDAO;
import com.ults.IPMS.entity.Login;
import com.ults.IPMS.entity.UserAccess;
import com.ults.IPMS.exception.ResourceNotFoundException;
import com.ults.IPMS.security.CustomUserDetails;
import com.ults.IPMS.service.UserAccessService;
import com.ults.IPMS.util.CommonResponse;
import com.ults.IPMS.util.JavaMail;
import com.ults.IPMS.util.Mail;

@Service("userService")
public class UserAccessServiceImpl implements UserAccessService, UserDetailsService {

	@Autowired
	UserAccessDAO userAccessDAO;

	@Autowired
	LoginDAO loginDAO;

	@Autowired
	BCryptPasswordEncoder encoder;

	@Autowired
	JavaMail javaMail;

	@Override
	public CommonResponse adduser(UserAccess user) {
		// TODO Auto-generated method stub
		if (userAccessDAO.existsByUsername(user.getUsername())) {
			return new CommonResponse("error", "username already exists");
		}

		userAccessDAO.save(user);

		/*********************/
		Login login = new Login();
		login.setUserName(user.getUsername());
		login.setUserAccess(user);
		login.setPassword(encoder.encode("password"));
		;
		/*********************/
		sendMail(user);

		return new CommonResponse("sucess", loginDAO.save(login), true);

	}

	public void sendMail(UserAccess user) {
		StringBuilder sb = new StringBuilder();
		sb.append("<b>Dear All");

		Mail mail = new Mail();
		mail.setFrom("sayeed.ap@ults.in");
		mail.setTo(user.getEmail());
		mail.setSubject("IPMS");
		mail.setSb("HI " + user.getFirstName() + ", you have been assigned as " + user.getRole()
				+ " in the project.Your username is " + user.getUsername());
		System.out.println(mail);
		javaMail.sendMail(mail);

	}

	@Override
	public CommonResponse updateuser(long id, UserAccess user) {
		UserAccess userAccessObj = userAccessDAO.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "user id", id));
		userAccessObj.setEmail(user.getEmail());
		return new CommonResponse("sucess", userAccessDAO.save(userAccessObj));

	}

	@Override
	public CommonResponse getuser(long id) {
		return new CommonResponse("sucess", userAccessDAO.findById(id).get());
	}

	@Override
	public CommonResponse delete(long id) {
		UserAccess userAccessObj = userAccessDAO.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "user id", id));
		
		userAccessDAO.delete(userAccessObj);
		return new CommonResponse("Success");
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username);
		UserAccess userAccess = userAccessDAO.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("Invalid username or password"));

		return new CustomUserDetails(userAccess.getUsername(), userAccess.getLogin().getPassword(),
				getAuthority(userAccess),userAccess);
	}

	private Collection<? extends GrantedAuthority> getAuthority(UserAccess userAccess) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		if (userAccess.getRole() == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}
		authorities.add(new SimpleGrantedAuthority(userAccess.getRole().getRole_name()));
		return authorities;
	}

	@Override
	public CommonResponse findAll(Pageable pageable) {
		return new CommonResponse("Success", userAccessDAO.findAll(pageable));
	}

}
