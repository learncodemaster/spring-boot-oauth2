package com.ults.IPMS.service.Impl;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ults.IPMS.dao.MenuServiceDAO;
import com.ults.IPMS.dao.RoleMenuAccessDAO;
import com.ults.IPMS.entity.Menu;
import com.ults.IPMS.entity.Role;
import com.ults.IPMS.entity.RoleMenuAccess;
import com.ults.IPMS.model.MenuDTO;
import com.ults.IPMS.service.MenuService;
import com.ults.IPMS.util.CommonResponse;

@Service
public class MenuServiceImp implements MenuService {
	
	@Autowired
	MenuServiceDAO menuServiceDao;
	
	@Autowired
	RoleMenuAccessDAO roleMenuAccessDao;

	@Override
	public CommonResponse insert(Menu menu) {
		return new CommonResponse("Success", menuServiceDao.save(menu), true);
	}

	@Override
	public CommonResponse loadMenu() {

		Role role = new Role();
		role.setId(1);
		
		//CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		

		Set<MenuDTO> menuDTOs = new HashSet<MenuDTO>();

		Set<RoleMenuAccess> col = roleMenuAccessDao.findByRole(role);


		col.forEach(roleMenuAccess->{
			MenuDTO menuDTO = new MenuDTO(roleMenuAccess);
			menuDTOs.add(menuDTO);
		});
		

		menuDTOs.forEach(mainMenu -> {
			mainMenu.getMenu().forEach(subMenu -> {
				menuDTOs.forEach(iteratingMenu -> {
					if (iteratingMenu.getMenuID() == subMenu.getMenuID()) {
						iteratingMenu.getPermissions().forEach(permission -> {
							subMenu.getPermissions().add(permission);
						});
					}
				});

			});
		});

		return new CommonResponse("Success", menuDTOs.stream().filter(m -> m.isMainMenu()).collect(Collectors.toSet()),
				true);
	}

}
