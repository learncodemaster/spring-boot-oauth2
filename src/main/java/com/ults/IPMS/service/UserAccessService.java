package com.ults.IPMS.service;

import org.springframework.data.domain.Pageable;

import com.ults.IPMS.entity.UserAccess;
import com.ults.IPMS.util.CommonResponse;

public interface UserAccessService {

	CommonResponse adduser(UserAccess user);

	CommonResponse updateuser(long id, UserAccess user);

	CommonResponse getuser(long id);

	CommonResponse delete(long id);

	CommonResponse findAll(Pageable pageable);

	
}
  