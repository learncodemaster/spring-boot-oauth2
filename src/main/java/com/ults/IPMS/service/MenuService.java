package com.ults.IPMS.service;

import com.ults.IPMS.entity.Menu;
import com.ults.IPMS.util.CommonResponse;

public interface MenuService {

	CommonResponse insert(Menu menu);

	CommonResponse loadMenu();

}
