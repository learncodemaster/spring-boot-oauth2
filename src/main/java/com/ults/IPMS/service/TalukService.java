package com.ults.IPMS.service;

import org.springframework.data.domain.PageRequest;

import com.ults.IPMS.entity.Taluk;
import com.ults.IPMS.util.CommonResponse;

public interface TalukService {

	CommonResponse insert(Taluk taluk);

	CommonResponse get(long id);

	CommonResponse  delete(long id);

	CommonResponse list(PageRequest of);
	

}
