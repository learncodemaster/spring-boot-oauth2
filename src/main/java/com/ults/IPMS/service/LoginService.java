package com.ults.IPMS.service;

import com.ults.IPMS.entity.Login;
import com.ults.IPMS.util.CommonResponse;

public interface LoginService {

	CommonResponse login(Login login);

	//CommonResponse history(LoginHistory loginHistory);

}
