package com.ults.IPMS.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.ults.IPMS.entity.Lookup;
import com.ults.IPMS.entity.LookupType;
import com.ults.IPMS.util.CommonResponse;

public interface DropDownService {

	CommonResponse insertLookup(Lookup lookup);

	CommonResponse insertLookupType(LookupType lookuptype);

	CommonResponse listAllCombo(List<String> types);

	CommonResponse findAll(Pageable pageable);

	CommonResponse typeFindAll(Pageable pageable);

	CommonResponse getLookup(long id);

	CommonResponse deleteLookup(long id);

	CommonResponse getType(long id);

	CommonResponse deleteType(long id);

}
