package com.ults.IPMS.service;

import org.springframework.data.domain.Pageable;

import com.ults.IPMS.entity.Property;
import com.ults.IPMS.util.CommonResponse;

public interface SurveyService {
	
	CommonResponse save(Property property);

	CommonResponse surveyReport(Pageable pageable);

}
