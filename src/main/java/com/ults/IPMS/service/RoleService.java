package com.ults.IPMS.service;

import com.ults.IPMS.entity.Role;
import com.ults.IPMS.entity.RoleLevel;
import com.ults.IPMS.util.CommonResponse;

public interface RoleService {

	CommonResponse insert(Role role);

	CommonResponse insertRoleLevel(RoleLevel roleLevel);

}
