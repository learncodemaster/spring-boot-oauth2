package com.ults.IPMS.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

public class Utility {
	
	public static String getToken() {
	    String token = null;
	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (authentication != null) {
	      token = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
	    }
	    return token;
	  }
}
