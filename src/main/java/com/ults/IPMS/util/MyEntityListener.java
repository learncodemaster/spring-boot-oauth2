package com.ults.IPMS.util;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.transaction.Transactional;

import org.springframework.security.core.context.SecurityContextHolder;

import com.ults.IPMS.entity.CommonFields;
import com.ults.IPMS.security.CustomUserDetails;

public class MyEntityListener {


	@PostConstruct
	public void myPostConstruct(CommonFields obj) {
	}

	@PreDestroy
	public void myPreDestroy(CommonFields obj) {
	}
	
	@PreRemove
	public void myPreRemove(CommonFields obj) {
		System.out.println("Deleted");
	}

	@PrePersist
	public void initialize(CommonFields obj) {

		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("@PrePersist:" + username);
		obj.setCreatedBy(username);

	}

	 @PreUpdate
	 @Transactional(dontRollbackOn = Exception.class)
	public void myPreUpdate(CommonFields obj) {
		CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("@PrePersist:" + userDetails.getUsername());
		obj.setModifiedBy(userDetails.getUsername());
	}

}
