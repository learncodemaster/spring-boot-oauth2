package com.ults.IPMS.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;


@Service
@Configuration
@EnableAsync
public class JavaMail extends Thread  {
	
	@Autowired
	private JavaMailSender sender;

	@Async
	public void sendMail(Mail mail) {

		MimeMessage message = sender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo(mail.getTo());
			helper.setText(mail.getSb().toString(), true);
			helper.setSubject(mail.getSubject());
			// helper.setFrom(mail.getFrom());

		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Error");
		}
		sender.send(message);
		System.out.println("Mail Sent Success!");

	}



}
