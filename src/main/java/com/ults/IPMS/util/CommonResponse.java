package com.ults.IPMS.util;

import java.io.Serializable;

public class CommonResponse implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long recordCount;
	private String message;
	private Object data;
	boolean status;
	boolean endIndex;

	public CommonResponse(String message) {
		super();
		this.message = message;
	}
	public CommonResponse() {

	}
	public CommonResponse(String message, Object data) {
		super();
	
		this.message = message;
		this.data = data;
		
	}
	
	

	public CommonResponse(String message, Object data,boolean status) {
		super();
		this.recordCount = 1;
		this.message = message;
		this.data = data;
		this.status = status;
		this.endIndex = true;
	}
	

	public CommonResponse(int recordCount, String message, Object data, boolean status,
			boolean endIndex) {
		super();
		this.recordCount=recordCount;
		this.message=message;
		this.data=data;
		this.status=status;
		this.endIndex=endIndex;
		// TODO Auto-generated constructor stub
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public long getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isEndIndex() {
		return endIndex;
	}

	public void setEndIndex(boolean endIndex) {
		this.endIndex = endIndex;
	}
	
	


}



