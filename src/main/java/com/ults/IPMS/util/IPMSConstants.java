package com.ults.IPMS.util;

public class IPMSConstants {
	
	public static final String DEFAULT_PAGE = "1";
	
	public static final String DEFAULT_LIMIT = "100";

	public static final String CLIENT_ID = "my-client";

	public static final String CLIENT_SECRET = "my-secret";

	public static final String authLink = "http://localhost:8080";
}
