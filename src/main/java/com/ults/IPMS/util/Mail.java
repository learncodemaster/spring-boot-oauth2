package com.ults.IPMS.util;

public class Mail {

		private String from;
		private String to;
		private String subject;
		private String sb;

		public Mail() {
			super();
		}

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getTo() {
			return to;
		}

		public void setTo(String to) {
			this.to = to;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getSb() {
			return sb;
		}

		public void setSb(String string) {
			this.sb = string;
		}

	}



