package com.ults.IPMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.UserAccess;
import com.ults.IPMS.service.UserAccessService;
import com.ults.IPMS.util.IPMSConstants;

@RestController
//@CrossOrigin("*")
public class UserAccessController {
	@Autowired
	UserAccessService userAccessService;

	@GetMapping("/user")
	private ResponseEntity<?> findAll(
			@RequestParam(name = "page", required = false, defaultValue = IPMSConstants.DEFAULT_PAGE) int page,
			@RequestParam(name = "limit", required = false, defaultValue = IPMSConstants.DEFAULT_LIMIT) int limit) {
		return ResponseEntity.ok().body(userAccessService.findAll(PageRequest.of(page - 1, limit)));
	}

	@GetMapping("/user/{id}")
	private ResponseEntity<?> getuser(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(userAccessService.getuser(id));
	}

	@PostMapping("/user")
	////@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	private ResponseEntity<?> adduser(@RequestBody UserAccess user) {
		return ResponseEntity.ok().body(userAccessService.adduser(user));
	}

	@PutMapping("/user/{id}")
	private ResponseEntity<?> updateuser(@PathVariable("id") long id,@RequestBody UserAccess user) {
		return ResponseEntity.ok().body(userAccessService.updateuser(id,user));

	}

	@DeleteMapping("/user/{id}")
	private ResponseEntity<?> delete(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(userAccessService.delete(id));

	}

}
