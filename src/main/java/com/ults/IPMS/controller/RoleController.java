package com.ults.IPMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.Role;
import com.ults.IPMS.entity.RoleLevel;
import com.ults.IPMS.service.RoleService;

@RestController
public class RoleController {
	
	@Autowired
	RoleService roleService;

	@PostMapping("/role")
	private ResponseEntity<?> insert(@RequestBody Role role) {
		return ResponseEntity.ok().body(roleService.insert(role));

	}

	@PostMapping("rolelevel")
	private ResponseEntity<?> insert1(@RequestBody RoleLevel roleLevel) {
		return ResponseEntity.ok().body(roleService.insertRoleLevel(roleLevel));

	}

}
