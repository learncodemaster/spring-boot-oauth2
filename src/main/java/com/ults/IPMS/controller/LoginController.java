package com.ults.IPMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.Login;
import com.ults.IPMS.service.LoginService;

@RestController
@CrossOrigin("*")
@RequestMapping("/login")
public class LoginController {
	@Autowired
	LoginService loginservice;
	
	@PostMapping("")
	private ResponseEntity<?>login(@RequestBody Login login){
		return ResponseEntity.ok().body(loginservice.login(login));
		
	}
	
  
//	@PostMapping("/history")
//	private ResponseEntity<?>history(@RequestBody LoginHistory loginHistory){
//		return ResponseEntity.ok().body(loginservice.history(loginHistory));
//		
//	}
}
