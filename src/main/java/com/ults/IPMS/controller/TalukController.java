package com.ults.IPMS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.dao.BuildingDAO;
import com.ults.IPMS.dao.PropertyDAO;
import com.ults.IPMS.entity.AdditionalDetails;
import com.ults.IPMS.entity.Building;
import com.ults.IPMS.util.IPMSConstants;

@RestController
public class TalukController {
	
	
	@Autowired
	BuildingDAO buildingDAO;
	
	@Autowired
	PropertyDAO PropertyDAO;
	
	@GetMapping("/taluk")
	private ResponseEntity<?> findAll(
			@RequestParam(name = "page", required = false, defaultValue = IPMSConstants.DEFAULT_PAGE) int page,
			@RequestParam(name = "limit", required = false, defaultValue = IPMSConstants.DEFAULT_LIMIT) int limit) {
		List<Object> apis = new ArrayList<Object>();
		apis.add(new AdditionalDetails());
		apis.add(new Building());
		
		return ResponseEntity.ok().body(PropertyDAO.findAll());
	}
	


}
