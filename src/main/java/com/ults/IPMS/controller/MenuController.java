package com.ults.IPMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.Menu;
import com.ults.IPMS.service.MenuService;
import com.ults.IPMS.util.Utility;

@RestController
public class MenuController {
	
	@Autowired
	MenuService menuService;
	
	@PostMapping("/menu")
	private ResponseEntity<?>insert(@RequestBody Menu menu){
		return ResponseEntity.ok().body(menuService.insert(menu));
		
	}
	
	@GetMapping("/menu")
	private ResponseEntity<?>loadMenu(){

				
		System.out.println("token:"+Utility.getToken());
		

		return ResponseEntity.ok().body(menuService.loadMenu());
		
		
	}
	
	
	
	
	
	
	
	

}
