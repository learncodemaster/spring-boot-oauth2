package com.ults.IPMS.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.Lookup;
import com.ults.IPMS.entity.LookupType;
import com.ults.IPMS.service.DropDownService;
import com.ults.IPMS.util.IPMSConstants;

@RestController
public class DropDownController {

	@Autowired
	DropDownService dropDownService;
	
	
	@GetMapping("/lookup")
	public ResponseEntity<?> findAll(
			@RequestParam(name = "page", required = false, defaultValue = IPMSConstants.DEFAULT_PAGE) int page,
			@RequestParam(name = "limit", required = false, defaultValue = IPMSConstants.DEFAULT_LIMIT) int limit) {
		return ResponseEntity.ok().body(dropDownService.findAll(PageRequest.of(page - 1, limit)));
	}

	@GetMapping("/lookup/{id}")
	public ResponseEntity<?> getLookup(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(dropDownService.getLookup(id));

	}
	
	@PostMapping("/lookup")
	public ResponseEntity<?> insertLookup(@RequestBody Lookup lookup) {
		return ResponseEntity.ok().body(dropDownService.insertLookup(lookup));
	}

	@DeleteMapping("/lookup/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(dropDownService.deleteLookup(id));
	}

	
	@GetMapping("/lookuptype")
	public ResponseEntity<?> typefindALL(
			@RequestParam(name = "page", required = false, defaultValue = IPMSConstants.DEFAULT_PAGE) int page,
			@RequestParam(name = "limit", required = false, defaultValue = IPMSConstants.DEFAULT_LIMIT) int limit) {
		return ResponseEntity.ok().body(dropDownService.typeFindAll(PageRequest.of(page - 1, limit)));
	}
	

	@GetMapping("/lookuptype/{id}")
	public ResponseEntity<?> getType(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(dropDownService.getType(id));
	}

	@PostMapping("/lookuptype")
	public ResponseEntity<?> insertLookupType(@RequestBody LookupType lookuptype) {
		return ResponseEntity.ok().body(dropDownService.insertLookupType(lookuptype));
	}

	@DeleteMapping("/lookuptype/{id}")
	public ResponseEntity<?> deleteType(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(dropDownService.deleteType(id));
	}

	/***List All dropdown list */
	@GetMapping("/listAllCombo")
	public ResponseEntity<?> listAll(@RequestParam(name = "types", required = false) List<String> types) {
		return ResponseEntity.ok().body(dropDownService.listAllCombo(types));
	}

}
