package com.ults.IPMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ults.IPMS.entity.Property;
import com.ults.IPMS.service.SurveyService;
import com.ults.IPMS.util.IPMSConstants;

@RestController
public class SurveyController {

	
	@Autowired
	private SurveyService surveyService;
	
	
	@PostMapping("/survey")
	private ResponseEntity<?>insert(@RequestBody Property property){
		return ResponseEntity.ok().body(surveyService.save(property));	
	}
	
	@GetMapping("/survey")
	private ResponseEntity<?>surveyReport(
			@RequestParam(name = "page", required = false, defaultValue = IPMSConstants.DEFAULT_PAGE) int page,
			@RequestParam(name = "limit", required = false, defaultValue = IPMSConstants.DEFAULT_LIMIT) int limit){
		return ResponseEntity.ok().body(surveyService.surveyReport(PageRequest.of(page-1, limit)));
		
	}
}
